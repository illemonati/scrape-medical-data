import { AxiosRequestConfig } from 'axios';

export const baseUrl = 'https://www.nhsinform.scot';
export const outputsPath = './outputs/';
export const outputFileName = 'outputs.txt';
export const outputJsonName = 'outputs.json';
export const axiosConfig: AxiosRequestConfig = {
    headers: {
        'User-Agent':
            'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.100 Safari/537.36 Edg/80.0.361.50',
    },
};
