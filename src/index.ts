import { fetchAllDiseases } from './getDiseases.js';
import { getDiseaseSections } from './getMoreInfo.js';
import {
    sleep,
    writeDiseaseToFile,
    initFile,
    convertOutputFileToJson,
} from './utils.js';
import { outputFileName, outputJsonName } from './config.js';
import { StandardSections } from './disease.js';

const main = async () => {
    initFile(outputFileName);
    const diseases = await fetchAllDiseases();
    for (const disease of diseases) {
        const f = async (depth?: number) => {
            depth = depth || 0;
            try {
                disease.sections = await getDiseaseSections(disease);
                disease.standardSections = new StandardSections(
                    disease.sections
                );
                writeDiseaseToFile(disease, outputFileName);
            } catch (e) {
                if (depth < 3) {
                    await sleep(500);
                    f(depth + 1);
                }
            }
        };
        f().then();
        await sleep(100);
    }
    convertOutputFileToJson(outputFileName, outputJsonName);
};

main().then();
