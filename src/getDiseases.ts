import cheerio from 'cheerio';
import axios from 'axios';
import { axiosConfig, baseUrl } from './config.js';
import Disease from './disease.js';
import { remove00A0 } from './utils.js';

const fetchRawPage = async () => {
    const url = baseUrl + '/illnesses-and-conditions/a-to-z';
    const resp = await axios.get(url, axiosConfig);
    return resp.data;
};

const getListFromRawPage = async (rawPage: string) => {
    const list = [] as Disease[];
    const parsed = cheerio.load(rawPage);
    parsed('.module__title').each((i, e) => {
        const title = parsed(e);
        const titleStr = title.text().trim();
        const parent = title.parent();
        const url = parent.attr('href');
        const description = parent.children('.module__desc');
        const descriptionStr = remove00A0(
            description.text().replace(/\n/g, ' ').replace(/\t/g, ' ')
        );
        list.push({
            name: titleStr || '',
            link: url || '',
            description: descriptionStr,
        });
    });
    return list;
};

export const fetchAllDiseases = async () => {
    const page = await fetchRawPage();
    return await getListFromRawPage(page);
};
