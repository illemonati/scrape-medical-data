import fs from 'fs';
import { outputsPath, outputJsonName } from './config.js';
import Disease from './disease.js';

export const sleep = (ms: number) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
};

export const initFile = (filename: string) => {
    if (!fs.existsSync(outputsPath)) {
        fs.mkdirSync(outputsPath);
    }
    fs.writeFileSync(outputsPath + filename, '');
};

export const writeDiseaseToFile = (disease: Disease, filename: string) => {
    fs.appendFileSync(outputsPath + filename, JSON.stringify(disease) + '\n');
};

export const remove00A0 = (input: string): string => {
    return input.normalize().replace('\u00A0', '');
};

export const convertOutputFileToJson = (
    outputFileName: string,
    jsonName: string
) => {
    const lines = fs
        .readFileSync(outputsPath + outputFileName, 'utf-8')
        .split('\n')
        .filter(Boolean);
    initFile(jsonName);
    fs.writeFileSync(outputsPath + outputJsonName, '[');
    lines.slice(0, lines.length - 2).forEach((line) => {
        fs.appendFileSync(outputsPath + outputJsonName, line + ',\n');
    });
    fs.appendFileSync(
        outputsPath + outputJsonName,
        lines[lines.length - 1] + '\n'
    );
    fs.appendFileSync(outputsPath + outputJsonName, ']');
};
