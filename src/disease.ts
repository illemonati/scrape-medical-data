export interface DiseaseSections {
    [key: string]: string;
}

/** class repersenting the standard sections */
export class StandardSections {
    about = {} as { [key: string]: string };
    symptoms = {} as { [key: string]: string };
    peopleAtRisk = {} as { [key: string]: string };

    constructor(diseaseSections: DiseaseSections) {
        Object.keys(diseaseSections).forEach((key: string) => {
            const lKey = key.toLowerCase();
            lKey.includes("about")
                ? (this.about[key] = diseaseSections[key])
                : lKey.match(/(who|risk|people)/)
                ? (this.peopleAtRisk[key] = diseaseSections[key])
                : lKey.match(/(signs|symptom|diagnos|complica|why)/) &&
                  (this.symptoms[key] = diseaseSections[key]);
        });
    }
}

export default interface Disease {
    name: string;
    link?: string;
    description?: string;
    sections?: DiseaseSections;
    standardSections?: StandardSections;
}
