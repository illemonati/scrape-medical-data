import cheerio from "cheerio";
import axios from "axios";
import { axiosConfig, baseUrl } from "./config.js";
import Disease, { DiseaseSections } from "./disease.js";
import { remove00A0 } from "./utils.js";

/** Get the sections for a given disease
 * @param {Disease} disease The disease to get the sections for
 * @returns {Promise<DiseaseSections>} It returns disease sections for the diseases you wanted sections for
 */
export const getDiseaseSections = async (
    disease: Disease
): Promise<DiseaseSections> => {
    const page = await getDiseaseInfoPage(disease);
    const parsedPage = cheerio.load(page);
    const editor = parsedPage(".editor").first();
    const sections: DiseaseSections = {};
    let sectionName = "About";
    sections[sectionName] = "";
    editor.children().each((i, e) => {
        const child = parsedPage(e);
        if (child[0].name === "h2") {
            sectionName = remove00A0(
                child.text().replace(/\n/g, " ").replace(/\t/g, " ")
            );
            sections[sectionName] = "";
        } else {
            sections[sectionName] += remove00A0(
                child.text().replace(/\n/g, " ").replace(/\t/g, " ")
            );
        }
    });
    return sections;
};

/** Gets the raw page for disease
 * @param {Disease} disease The input disease
 *  @returns {Promise<string>} the raw page output
 */
const getDiseaseInfoPage = async (disease: Disease): Promise<string> => {
    const url = baseUrl + disease.link;
    const resp = await axios.get(url, axiosConfig);
    return resp.data;
};
